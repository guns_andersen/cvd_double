// ==UserScript==
// @name Chatvtroem
// @namespace Chatvdvoem
// @include http://chatvdvoem.ru/
// @version 2.5.10
// @grant none
// @downloadURL https://bitbucket.org/2ch_dev/cvd_double/raw/master/chatvtroem.user.js
// @description 
// @encoding utf-8
// ==/UserScript==

String.prototype._repeat = function (count) {
    var result = '';
    while (count--) {
        result += this;
    }
    return result;
}

var settings = {
    'auto_reply': true,
    'auto_disconnect': true,
    'auto_restart': true,
    'symbol': '\t',
    'anonymous': false,
    'write_chat_log' : false
};

var filterWords = ['vk','id'];

var anon_count = [];
var update_anon_count;

var chat_log = '';

function connect(from, to, side) {
    from.initBz1 = function () {};
    var process_event = from.process_event;
    var last_message_by_anon = false;

    from.process_event = function (data) {
        var result = process_event(data);
        if (data.action == 'start_chat') {
            anon_count[side] = 0;
            update_anon_count();
            last_message_by_anon = false;
        }
        if (data.action == 'start_chat' && from.chat_cid == to.chat_cid) {
            from.onbeforeunload = to.onbeforeunload = undefined;
            start();
        } else if (data.action == 'new_message') {
            var message = from.document.querySelector('#messages').firstChild.lastChild;
            var message_content = message.querySelector('.message').textContent;

            var message_menu = document.createElement('span');
            message_menu.className = 'message_menu';
            message.appendChild(message_menu, message.firstChild);

            var resend_button = add_button(message_menu, "отпр", function () {
                if (chatFilter(message_content) == true) {
                    if (message_content.lastIndexOf("!no_send") == -1) {
                        if (settings['anonymous'] == true) {
                            to.send('send_message', {
                                message: message_content
                            });
                        } else {
                            to.send('send_message', {
                                message: message_content + settings['symbol']
                            });
                        }
                        to.chat_ping_send = Date().getTime();
                    } else {
                        console.log("Полученно сообщение от анона");
                    }
                }
            });
            add_button(message_menu, "ред", function () {
                to.$('#text').val(message_content).focus();
            });

            if (data.user == 'im') {
                var log_message_author;
                var log_color;
                if (last_message_by_anon) {
                    message.querySelector('.name').innerHTML = '<i>Анон</i>';
                    message.className = 'messageAnon';
                    last_message_by_anon = false;
                    log_message_author = 'Анон собеседнику ' + (side + 1);
                    log_color = (side==0?'#0f0':'#0b0');
                } else {
                    message.querySelector('.name').innerHTML = '<i>Сюда</i>';
                    log_message_author = 'Собеседник ' + (2 - side);
                    log_color = (side==0?'#CA3B1B':'#348ABC');
                }
                console.log('loger');
                if (settings['write_chat_log']) {
                    chat_log += '<span style="color: white; background-color:' + log_color + ';">' + log_message_author
                     + '</span> ' + message_content + '<br/>';
                     console.log("loged");
                }
            } else {
                message.querySelector('.name').innerHTML = '<i>Отсюда</i>';

                // говнокод для подсчёта количества tabов в конце
                // надо чем нибудь нормальным заменить
                var symbols_it = message_content.length - 1;
                while (symbols_it >= 0 && message_content.charAt(symbols_it) == settings['symbol']) {
                    symbols_it -= 1;
                }
                var symbols_count = message_content.length - symbols_it - 1;

                if (symbols_count != anon_count[side]) {
                    anon_count[side] = symbols_count;
                    update_anon_count();
                }

                if (settings['auto_reply'] == true) {
                    resend_button.onclick();
                }

            }
        }
           
//        } else if (data.action == 'start_typing' && !to.chat_typing_status) {
//            to.send('start_typing');
//            to.chat_ping_send = Date().getTime();
//        } else if (data.action == 'stop_typing' && to.chat_typing_status) {
//            to.send('stop_typing');
//            to.chat_ping_send = Date().getTime();
        else if (data.action == 'stop_chat' && data.user != 'im') {
            on_chat_stop({});
        }
        return result;
    };

    var chat_stop = from.chat_stop;
    from.chat_stop = function (opponent_init, script_params) {
        if (!script_params) script_params = {};
        on_chat_stop(script_params);
        return chat_stop(opponent_init);
    }

    function on_chat_stop(script_params) {
        if (settings['write_chat_log']) {
            chat_log += '<span style="color:#fff; background-color:#999;">Собеседник ' + (side + 1) + ' ушёл от нас</span><br/>';
        }
        if ((settings['auto_disconnect'] == true && !script_params.auto_disc_init) ||
                script_params.force_auto_disconnect) {
            to.chat_stop(false, {
                force_restart: script_params.force_restart,
                auto_disc_init: true
            });
        }
        if (settings['auto_restart'] == true || script_params.force_restart) {
            safe_chat_start();
        }

    }

    function migalka(newTxt) {
        var oldTxt = document.title;
        
        if (document.title == oldTxt) {
            document.title = '** ' + newTxt + ' **';
        } else {
            document.title = oldTxt;
        }
    }

    function chatFilter(msg) {
        var q = false;
        
        for (var i = 0; i < filterWords.length; i++) {
            if (msg.lastIndexOf(filterWords[i]) != -1) {
                q = true;
                break;
			}
        }
        
        if (!q) {
            clearInterval(timer);
            return true;
        } else {
//            settings['auto_reply'] = false;
            var timer = setInterval(migalka('Аутист дал свои контакты'), 800);
            return false;
        }
    }

    function safe_chat_start() {
        setTimeout(function() {
            if (from.chat_status == 'disconnect') from.chat_start();
        }, 1600);
    }

    from.$("#message_form").unbind().submit(function () {
        if (!from.chat_cid) {
            return false;
        }
        last_message_by_anon = true;
        from.send("send_message", {
            message: from.$("#text").val().replace(/\t/g, '') + '\t'._repeat(anon_count[side == 0 ? 1 : 0] + 1)
        });
        var chat_time = new Date();
        from.chat_ping_send = chat_time.getTime();
        from.$("#text").val('').focus();
        return false;
    });

    from.chat_start();
};

function create_menu(one, two) {
    var menu = document.createElement('div');
    menu.id = 'menu';

    // Switches
    add_switch(menu, 'auto_reply', '[✔] Пересылать', '[✘] Пересылать');
    add_switch(menu, 'auto_disconnect', '[✔] Отключать автоматически', '[✘] Отключать автоматически');
    add_switch(menu, 'auto_restart', '[✔] Авторестартить', '[✘] Авторестартить');
    add_switch(menu, 'anonymous', '[✔] Анонимно', '[✘] Анонимно');
    add_switch(menu, 'write_chat_log', '[✔] Лог чата', '[✘] Лог чата');
    // Buttons
    add_button(menu, 'Смотреть лог', function () {
        window.open('data:text/html;charset=utf-8,' + encodeURIComponent(chat_log));
    });
    add_button(menu, 'Очистить лог', function () {
        chat_log = '';
    });
    add_button(menu, 'Рестарт', function () {
        one.contentWindow.chat_stop(false, {
            force_restart: true,
            force_auto_disconnect: true
        });
    });

    create_anon_indicator(menu);
  

    document.body.appendChild(menu);
};

function add_button(parent, name, f) {
    var button = document.createElement('a');
    button.setAttribute('href', '#');
    button.textContent = name;
    button.classList.add('menu_item');

    button.onclick = function () {
        f(button);
        return false;
    };

    parent.appendChild(button);
    return button;
};

function add_switch(parent, settings_key, on_true, on_false, change_callback) {
    var text;

    if (settings[settings_key] == true) {
        text = on_true;
    } else {
        text = on_false;
    }

    add_button(parent, text, function (e) {
        settings[settings_key] = !settings[settings_key];

        if (settings[settings_key] == true) {
            e.textContent = on_true;
        } else {
            e.textContent = on_false;
        }
        if (change_callback) change_callback();
    });
};

function create_anon_indicator(parent) {
    var indicator = document.createElement('span');
    indicator.classList.add('menu_item');

    anon_count = [0, 0];

    update_anon_count = function () {
        indicator.innerHTML = "Счётчик анонов: " + anon_count[0] + "-" + anon_count[1];
    }
    parent.appendChild(indicator);
    update_anon_count();
}

function start() {

    function load_style(doc) {
        var style = doc.createElement('link');
        style.setAttribute('href', 'https://bitbucket.org/2ch_dev/cvd_double/downloads/style.css');
        style.setAttribute('rel', 'stylesheet');
        style.setAttribute('type', 'text/css');
        style.setAttribute('media', 'screen');
        doc.head.appendChild(style);
    }

    var frameset = document.createElement('frameset');
    var one = document.createElement('frame');
    var two = document.createElement('frame');
    frameset.setAttribute('cols', '50%, 50%');
    one.addEventListener('load', function (e) {
        two.setAttribute('src', '/?2');
        load_style(one.contentWindow.document); 
    });
    two.addEventListener('load', function (e) {
        connect(one.contentWindow, two.contentWindow, 0);
        connect(two.contentWindow, one.contentWindow, 1);
        load_style(two.contentWindow.document);
    });
    one.setAttribute('src', '/?1');
    frameset.appendChild(one);
    frameset.appendChild(two);

    document.body.innerHTML = document.head.innerHTML = '';

    load_style(document);

    create_menu(one, two);

    document.body.appendChild(frameset);
};

(function () {
    var activate = document.createElement('a');
    activate.setAttribute('href', '/');
    activate.textContent = 'Траллеть';
    activate.addEventListener('click', function (e) {
        start();
        e.preventDefault();
    });
    document.querySelector('.startChat').appendChild(activate);
})();
